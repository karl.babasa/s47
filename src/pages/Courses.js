import React from 'react';
import coursesData from '../mockData/coursesData';
import CourseCard from '../components/CourseCard'

export default function Courses() {
	//for us to e able to display all the courses from the data file, we are going to use map()
	//The "map" method loops through all the indevidual course object in our array and return a component for each course.
	//Multiple components created through the map method mush have aUNIQUE KEY that will help React JS identify which components/elements have been changed, added or removed.
	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id}courseProp={course}/>
			)
	})
	
	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
		)
}