import React, {useState, useEffect} from 'react';
import	{Form, Button} from	'react-bootstrap';
import Swal from 'sweetalert2';

export default function	Login() {

	const [email, setEmail] = useState('');
	const [password , setPassword] = useState('');
	
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button when all field are papulated and both passwords match
		if((email !== '' && password !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function loginUser(e) {
		//Prevents page redirection via form submission
		e.preventDefault();

		//clear input
		setEmail('')
		setPassword('')

		Swal.fire({
			title: "Login",
			icon: "success",
			text: "Login: True"
		})
	}

	return(
		<Form onSubmit={(e) => loginUser(e)}>
			<h1>Login</h1>

			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter Email Address"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter Password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>

				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
		</Form>
		)

}