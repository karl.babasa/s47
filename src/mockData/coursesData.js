const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quos tempora nemo suscipit laborum, recusandae, obcaecati velit fuga illo quisquam eaque nisi, ipsum quia voluptates placeat quasi culpa explicabo minima nam?",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phyton - Django",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quos tempora nemo suscipit laborum, recusandae, obcaecati velit fuga illo quisquam eaque nisi, ipsum quia voluptates placeat quasi culpa explicabo minima nam?",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quos tempora nemo suscipit laborum, recusandae, obcaecati velit fuga illo quisquam eaque nisi, ipsum quia voluptates placeat quasi culpa explicabo minima nam?",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;